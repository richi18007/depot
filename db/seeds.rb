# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Product.delete_all 

Product.create!( title: 'Programming ruby 1.9 and 2.0' , 
	description: %{<p>
		Ruby is the fastest growing and one of the most dyanamic languages out there.
		If you need to have your program delivered fast and in iterations , ruby is
		what you should be looking for 	
	</p>} , 
	price: 49.95 ,
	image_url: 'ruby.png'	
)

Product.create!( title: 'Programming Javascript' ,
        description: %{<p>
        	Javascript is one of the most popular programming languages in the world today
	</p>} ,
        price: 59.95 ,
        image_url: 'js.png'
)

Product.create!( title: 'Programming C' ,
        description: %{<p>
        	C is the oldest and according to some people still the best programming language around
	</p>} ,
        price: 29.95 ,
        image_url: 'c++.jpg'
)
 
