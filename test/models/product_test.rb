require 'test_helper'

class ProductTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  fixtures :products 

  test "product attributes must not be empty" do 
		product = Product.new 
		assert product.invalid?
		assert product.errors[:title].any?
		assert product.errors[:description].any?	
		assert product.errors[:price].any?	
		assert product.errors[:image_url].any?	
	end

	test "product price must be valid" do
		product = Product.new(
			title: "Lorem Ipsum" , 
			description: "something " , 
			image_url: "zz.jpg"
		)	
		product.price=-1;
		assert product.invalid?
		assert_equal ["must be greater than or equal to 0.01"] , 
			product.errors[:price]
		product.price = 1 
		assert product.valid?
	end 
	
	test "Image url" do
		ok = %w{ fred.jpg fred.gif fred.png 
						http://a.b.c/d.f.e/fred.jpg
		}

		bad = %w{ fred.doc fred fred.xsl deed.gif.more 
		}

		ok.each do |name| 
			product = new_product(name)
			assert product.valid? , "#{name} should be valid"
		end

		bad.each do |name|
			product = new_product(name)
			assert product.invalid? , "#{name} should be invalid"
		end
	end

	def new_product(image_url)
		product = Product.new(
                        title: "Lorem Ipsum" ,
                        description: "something " ,
                        image_url: image_url , 
			price: 1.0 
                )
	end

	test "Product is not valid without a unique title" do	
	product = Product.new(
                        title: products(:ruby).title ,
                        description: "something " ,
                        image_url: "zz.jpg" , 
			price: 1.0
                )		
	assert product.invalid?
	assert_equal ["has already been taken"] , product.errors[:title]
	end
end
